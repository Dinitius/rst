import styled from 'styled-components'

export const MainContainer = styled.div`
  width: 1060px;
  margin: auto;
  padding: 50px 0;
  font-family: 'Inter', sans-serif;
`
export const Head = styled.div`
  text-align: left;
  margin-bottom: 20px;
`
export const Title = styled.div`
  font-weight: 800;
  margin-bottom: 10px;
  font-size: 24px;
  line-height: 100%;
`
export const SubTitle = styled.div`
  font-size: 14px;
  line-height: 150%;
  color: #636E72;
`
export const Tabs = styled.div`
  display: flex;
  justify-content: flex-start;
  margin-bottom: 20px;
`

export const Tab = styled.div`
  display: flex;
  padding: 13px 15px;
  font-weight: 500;
  font-size: 14px;
  color: #495458;
  background: #FCFCFC;
  border: 1px solid #DADCE0;
  box-sizing: border-box;
  border-radius: 33px;
  margin-right: 10px;
  cursor: pointer;
`

export const ActiveTab = styled(Tab)`
  color: #387CCC;
  border: 1px solid #387CCC;
`

export const Cards = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
`
export const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin-bottom: 20px;
  width: 340px;
  height: 314px;
  background: #FCFCFC;
  border: 1px solid #DADCE0;
  border-radius: 5px;
  text-align: left;
  :not(:nth-child(3n)){
    margin-right: 20px;
}
`

export const CardImage = styled.div`
  width: 340px;
  height: 170px;
  overflow-y: hidden;
`
export const Image = styled.img`
  width: 100%;

`

export const CardDescription = styled.div`
  height: 144px;
  width: 340px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  text-align: left;
`

export const Tags = styled.div`
  z-index: 2;
  position: relative;
  top: -188px;
  left: 10px;
  max-width: 300px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: #FCFCFC;
`

export const Tag = styled.div`
  background-color: ${props => props.color};
  border: 1px solid #FCFCFC;
  font-size: ${props => props.itemProp};
  height: 34px;
  border-radius: 20px;
  margin-right: 10px;
  padding: 10px;
  display: flex;
  align-items: center;
`
export const Location = styled.div`
  font-size: 12px;
  margin-bottom: 5px;
`
export const Date = styled.div`
  font-size: 12px;
  margin-bottom: 5px;
`
export const CardTitle = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 150%;
  margin-bottom: 5px;
`

export const CardPrice = styled.div`
  position: relative;
  bottom: 20px;
  left: 20px;
  font-weight: 800;
`
export const Discount = styled.div`
  text-decoration: line-through;
  font-size: 14px;
  font-weight: 500;
  position: relative;
  bottom: 15px;
  left: 20px;
`