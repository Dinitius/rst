import React from 'react'
import './App.css';
import {MainContainer} from './styled/content';
import Header from "./features/Header/Header";
import TabsList from "./features/CardList/Tabs";
import CardList from "./features/CardList/CardList";

const App = (): React.ReactElement => {
    return (
        <MainContainer className="App">
            <Header/>
            <TabsList/>
            <CardList/>
        </MainContainer>
    );
}

export default App;
