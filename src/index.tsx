import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const AppWrapper = () => {
    return (
        <App/>
    )
}
ReactDOM.render(
    <AppWrapper />, document.getElementById('root'))

reportWebVitals();
