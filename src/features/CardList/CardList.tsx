import React from "react";
import {createEffect} from "effector/effector.umd";
import {CardProps, CardsProps, ICard, TypeDate} from "../../interfaces/interfaces";
import {createEvent, createStore} from "effector";
import {useStore} from "effector-react";
import {
    Card,
    CardDescription,
    CardImage,
    CardPrice,
    Cards,
    CardTitle,
    Date,
    Discount,
    Image,
    Location,
    Tag,
    Tags
} from "../../styled/content";
import {wordDeclination} from '../../utils'
import {clearCards} from "./Tabs";
import Loader from "../loadre";
export const getCards = createEffect(async ({name, value}: CardProps) => {
    const req = await fetch(`https://rsttur.ru/api/collections/author-tour?city_id=48&count=12&${name}=${value}`)
    return req.json()
})
const $cards = createStore<CardsProps | []>([]).on(getCards.doneData, (state, data) => data)
const CardList = () => {
    $cards.reset(clearCards)
    const cards: CardsProps | [] = useStore($cards)
    if ("data" in cards) {
        return <Cards>
            {cards.data.map((card: ICard, index: number) => {
                return <Card key={index}>
                    <CardImage>
                        <Image src={`https://rsttur.ru/${card.image.md}`} alt="photo"/>
                        <Tags>
                            {card.price && card.price.discount !== undefined &&
                            <Tag color={'#F4302A'} itemProp={'14px'}>{`-${card.price.discount}%`}</Tag>}
                            {card.online ?
                                <Tag color={'rgba(50, 195, 135, 0.7)'} itemProp={'10px'}>{`Покупка онлайн`}</Tag> :
                                <Tag color={'rgba(251, 140, 0, 0.7)'} itemProp={'10px'}>{`Покупка по заявке`}</Tag>}
                        </Tags>
                    </CardImage>
                    <CardDescription>
                        <CardTitle>
                            {card.title}
                        </CardTitle>
                        <Location>
                            {card.location}
                        </Location>
                        <Date>
                            {card.date.typeDate === TypeDate.days_week && card.date.date.map((item, index) => {
                                return card.date.date.length > 1 ?
                                    index !== card.date.date.length - 1 ? `${item}, ` : item : item
                            })}
                            {card.date.typeDate === TypeDate.date && <span>{`${card.date.date}`}</span>
                            }
                            {card.duration && card.duration.hour && <span>{` (${card.duration.hour}
                            ${wordDeclination(card.duration.hour, ["час", "часа", "часов"])})`}</span>}
                            {card.duration && card.duration.day && <span>{` (${card.duration.day} 
                            ${wordDeclination(card.duration.day, ["день", "дня", "дней"])})`}</span>}
                        </Date>
                    </CardDescription>
                    {card.price && card.price.factPrice > card.price.price &&
                    <Discount>{card.price.factPrice}&#8381;</Discount>}
                    {card.price && <CardPrice>{card.price.price}&#8381;</CardPrice>}
                </Card>
            })}
        </Cards>
    }
    return <Loader />
}

export default CardList