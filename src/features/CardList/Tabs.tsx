import React, {useState} from "react";
import {createEvent, createStore, createEffect} from 'effector'
import {ActiveTab, Tab, Tabs} from "../../styled/content";
import ITab, {TabsProps} from "../../interfaces/interfaces";
import {useStore} from "effector-react";
import {getCards} from "./CardList";

const getTabs = createEffect(async () => {
    const req = await fetch(`https://rsttur.ru/api/collections/author-tour-region-tabs?city_id=48`)
    return req.json()
})
export const clearCards = createEvent()
const changeTab = createEvent<number>()
const $tabs = createStore<TabsProps | []>([]).on(getTabs.doneData, (state, data) => data)
getTabs()
const $active = createStore<number>(0)

const TabsList = (): React.ReactElement => {
    const changeDef = (state: number, id: number) => {
        state = id
        return state
    }
    $active.on(changeTab, changeDef)
    const tabs: TabsProps | [] = useStore($tabs)
    const isActive = useStore($active)
    const [needDefault, setNeedDefault] = useState(true)
    if ("data" in tabs) {
        const defaulTab = tabs.data.find(i => i.active)
        if (defaulTab && needDefault) {
            changeTab(defaulTab.id)
            getCards(defaulTab.params[0])
            setNeedDefault(false)
        }
        return <Tabs>
            {tabs.data.map((tab: ITab, index: number) => {
                return isActive === tab.id ? <ActiveTab key={index}>{tab.name}</ActiveTab> : <Tab onClick={() => {
                    changeTab(tab.id)
                    clearCards()
                    getCards(tab.params[0])
                    console.log(tab.params[0])
                }
                } key={index}>{tab.name}</Tab>
            })}
        </Tabs>
    }
    return <></>
}

export default TabsList