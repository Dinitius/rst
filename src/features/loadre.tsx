import React from 'react'
import './loader.css'

const Loader = () => {
    return <div>
        <div className="row">
            <div className="container">
                <div className="grid-row grid-4-4">
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                    <div className="cards">
                        <div className="card_image loading"></div>
                        <div className="card_title loading"></div>
                        <div className="card_description loading"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Loader