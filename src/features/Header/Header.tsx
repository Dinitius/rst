import React from "react";
import {Head, Title, SubTitle} from "../../styled/content";

const Header = (): React.ReactElement => {
    const headerData = {
        "title": "Авторские туры по регионам",
        "color": "warning",
        "description": [
            "От похода в гору до пешеходной прогулки.",
            "От круиза до погружения в виртуальную реальность. Туры для каждого."
        ]
    }
    return <Head>
        <Title>{headerData.title}</Title>
        {headerData.description.map((item: string, index: number) => {
            return <SubTitle key={index}>{item}</SubTitle>
        })}
    </Head>
}

export default Header