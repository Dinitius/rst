export default interface ITab {
    id: number
    active: boolean
    name: string
    params: CardProps[]
}

export interface CardProps {
    name: string
    value: number
}

export interface IDate{
    date: string[]
    end?: string
    typeDate: string
}

export interface IDuration{
    hour?: number
    day?: number
}

export interface ICard {
    id: number
    url: string
    image: IPhoto
    title: string
    location: string
    price: IPrice
    online?: boolean
    date: IDate
    duration: IDuration
}

export interface IPrice {
    currency: string
    discount?: number
    factPrice: number
    price: number
    typePrice: string
}

export interface IPhoto{
    sm: string
    md: string
    lg: string
}

export interface ICards {
    data: ICard[]
}

export interface ITabs {
    data: ITab[]
}

export interface TabsProps {
    error: null
    data: ITab[]
    success: boolean
    time: string
}

export interface CardsProps {
    error: null
    data: ICard[]
    success: boolean
    time: string
}

export enum TypeDate {
    date="date",
    days_week="days_week"
}